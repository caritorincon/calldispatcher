package com.almundo.java.exam.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {

    private static final Logger logger = LogManager.getLogger();
    private static Properties prop = new Properties();

    private static final String POOL_PROPERTIES = "pool.properties";

    static {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream resourceStream = loader.getResourceAsStream(POOL_PROPERTIES)) {
            prop.load(resourceStream);
        } catch (IOException e) {
            logger.error("Error loading properties", e);
        }

    }

    public static int getIntValue(String key, String def) {
        return Integer.parseInt(prop.getProperty(key, def));
    }

    public enum Key {
        CORE_POOL_SIZE("corePoolSize", "0"),
        SEC_KEEP_ALIVE("secondsKeepAlive", "5"),
        CAP_BLOKING_QUEUE("capacityBlockingQueue", "5"),
        MAX_POOL_SIZE("maxPoolSize", "10");

        String key;
        String defValue;

        Key(String key, String defValue) {
            this.key = key;
            this.defValue = defValue;
        }

        public String getKey() {
            return key;
        }

        public String getDefValue() {
            return defValue;
        }
    }
}