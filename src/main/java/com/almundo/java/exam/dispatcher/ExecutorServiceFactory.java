package com.almundo.java.exam.dispatcher;

import com.almundo.java.exam.util.PropertiesManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.almundo.java.exam.util.PropertiesManager.Key;

public class ExecutorServiceFactory {
    private static final Logger logger = LogManager.getLogger();
    private static Properties prop = new Properties();

    static {
        String resourceName = "pool.properties"; // could also be a constant
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
            prop.load(resourceStream);
        } catch (IOException e) {
            logger.error("Error loading properties", e);
        }

    }

    public static ExecutorService getExecutorService() {
        return createPool();
    }

    private static ExecutorService createPool() {

        return new ThreadPoolExecutor(PropertiesManager.getIntValue(Key.CORE_POOL_SIZE.getKey(), Key.CORE_POOL_SIZE.getDefValue()),
                PropertiesManager.getIntValue(Key.MAX_POOL_SIZE.getKey(), Key.MAX_POOL_SIZE.getDefValue()),
                PropertiesManager.getIntValue(Key.SEC_KEEP_ALIVE.getKey(), Key.SEC_KEEP_ALIVE.getDefValue()),
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(PropertiesManager.getIntValue(Key.CAP_BLOKING_QUEUE.getKey(), Key.SEC_KEEP_ALIVE.getDefValue())));
    }
}
