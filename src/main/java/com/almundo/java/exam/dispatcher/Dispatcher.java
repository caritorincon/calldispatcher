package com.almundo.java.exam.dispatcher;

import com.almundo.java.exam.call.Call;
import com.almundo.java.exam.call.CallProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

public class Dispatcher implements IDispatcher {
    private static final Logger logger = LogManager.getLogger();
    private ExecutorService executor;

    public Dispatcher(ExecutorService executor) {
        this.executor = executor;
    }

    @Override
    public void dispatchCall(Call call) {
        logger.info("a new call is entering {}", call.getId());
        try {
            executor.submit(new CallProcessor(call));
        } catch (RejectedExecutionException e) {
            logger.error("The system could'n process the call : Call {} Rejected ", call.getId());
            call.setStatus(Call.Status.REJECTED_MAX);
        }
    }

    @Override
    public void shutdown() {
        executor.shutdown();
        logger.info("Dispatcher is shouting down");
    }

    @Override
    public boolean isAllCallsDone() {
        return !executor.isTerminated();
    }
}
