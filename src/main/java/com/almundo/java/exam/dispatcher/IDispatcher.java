package com.almundo.java.exam.dispatcher;

import com.almundo.java.exam.call.Call;


public interface IDispatcher {
    /**
     * Processes the Call
     *
     * @param call call info
     */
    void dispatchCall(Call call);

    /**
     * Stops the call process
     */
    void shutdown();

    /**
     * Checks if all the calls have been processed after the shutdown
     *
     * @return true if all the calls have been processed
     */
    boolean isAllCallsDone();
}
