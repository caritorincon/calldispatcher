package com.almundo.java.exam.handler;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Define who is available to handle the call
 */
public class CallHandlerManager {
    private static final Logger logger = LogManager.getLogger();

    // Number of employees per Job
    static private int[] available = new int[]{4, 2, 1};
    // Define the handler type order
    static private List<Handler> order = new ArrayList<>(Arrays.asList(Handler.OPERATOR, Handler.SUPERVISOR, Handler.DIRECTOR));

    /**
     * Searches an available employee to handle the call
     *
     * @return the handler type if there is an available employee
     * @throws NoCallHandlerAvailable when There is not an available call handler
     */
    public static String getAvailableCallHandler() throws NoCallHandlerAvailable {
        Optional optional = order.stream().filter(CallHandlerManager::isAvailable).findFirst();
        if (optional.isPresent()) {
            logger.trace("Available handler {}", optional.get().toString());
            return optional.get().toString();
        }
        throw new NoCallHandlerAvailable();
    }

    /**
     * Release a Handler and update the counters
     *
     * @param handlerType type of handler to release
     */
    public static synchronized void releaseHandler(String handlerType) {
        Handler h = Handler.valueOf(handlerType);
        available[h.index]++;
        logger.debug("Release handler {} [{}]", handlerType, available[h.index]);
    }

    private static synchronized boolean isAvailable(Handler h) {
        if (available[h.index] > 0) {
            available[h.index]--;
            return true;
        }
        logger.trace("No Available handler {}", h.toString());
        return false;
    }

    private enum Handler {
        OPERATOR(0), SUPERVISOR(1), DIRECTOR(2);

        private int index;

        Handler(int index) {
            this.index = index;
        }
    }
}
