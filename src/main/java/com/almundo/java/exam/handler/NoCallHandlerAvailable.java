package com.almundo.java.exam.handler;

public class NoCallHandlerAvailable extends Exception {
    public NoCallHandlerAvailable() {
        super("There is not an available call handler");
    }
}
