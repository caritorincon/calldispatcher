package com.almundo.java.exam.call;

import com.almundo.java.exam.handler.CallHandlerManager;
import com.almundo.java.exam.handler.NoCallHandlerAvailable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Contains the logic of processing a call.
 */
public class CallProcessor implements Runnable {
    private static final Logger logger = LogManager.getLogger();
    private static final int MIN_CALL_DURATION = 5;
    private static final int MAX_CALL_DURATION = 10;

    private Call call;

    public CallProcessor(Call call) {
        this.call = call;
    }

    public void run() {
        logger.info("Processing call {}", call.getId());
        if (findHandler()) {
            process();
            releaseHandler();
        }

        logger.info("End Process of call {} [{}]", call.getId(), call.getStatus());
    }

    private boolean findHandler() {
        try {
            call.setCallHandler(CallHandlerManager.getAvailableCallHandler());
            return true;
        } catch (NoCallHandlerAvailable noCallHandlerAvailable) {
            logger.error("Error finding handler for the call {} .{}", call.getId(), noCallHandlerAvailable.getMessage());
            call.setStatus(Call.Status.REJECTED_NO_HANDLER);
        }

        return false;
    }

    private void process() {
        try {
            // process the call
            TimeUnit.SECONDS.sleep(getCallSeconds());
            call.setStatus(Call.Status.ANSWERED);

        } catch (InterruptedException e) {
            logger.error("Error Processing the call {} ", call.getId(), e);
            call.setStatus(Call.Status.REJECTED_NO_HANDLER);
        }
    }

    private void releaseHandler() {
        CallHandlerManager.releaseHandler(call.getCallHandler());
    }

    private int getCallSeconds() {
        return ThreadLocalRandom.current().nextInt(MIN_CALL_DURATION, MAX_CALL_DURATION + 1);
    }
}
