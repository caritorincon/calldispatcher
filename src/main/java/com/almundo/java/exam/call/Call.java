package com.almundo.java.exam.call;

/**
 * Represents the info of a Call
 */
public class Call {
    private String id;
    private Status status = Status.PENDING;
    private String callHandler;

    public Call(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCallHandler() {
        return callHandler;
    }

    public void setCallHandler(String callHandler) {
        this.callHandler = callHandler;
    }

    public enum Status {
        PENDING, ANSWERED, REJECTED_NO_HANDLER, REJECTED_MAX
    }

}
