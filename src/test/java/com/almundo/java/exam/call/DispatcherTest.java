package com.almundo.java.exam.call;

import com.almundo.java.exam.dispatcher.Dispatcher;
import com.almundo.java.exam.dispatcher.ExecutorServiceFactory;
import com.almundo.java.exam.dispatcher.IDispatcher;
import com.almundo.java.exam.util.PropertiesManager;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class DispatcherTest {
    private IDispatcher dispatcher;
    private static int numCall;
    private static int numWaiting;

    @BeforeAll
    static void loadProperties() {
        numCall = PropertiesManager.getIntValue(PropertiesManager.Key.MAX_POOL_SIZE.getKey(),
                PropertiesManager.Key.MAX_POOL_SIZE.getDefValue());

        numWaiting = PropertiesManager.getIntValue(PropertiesManager.Key.CAP_BLOKING_QUEUE.getKey(),
                PropertiesManager.Key.CAP_BLOKING_QUEUE.getDefValue());
    }

    @BeforeEach
    void setUp() {
        dispatcher = new Dispatcher(ExecutorServiceFactory.getExecutorService());
    }

    @AfterEach
    void tearDown() {
        dispatcher.shutdown();
    }

    @Test
    void dispatchCallTest() {
        List<Call> calls = new ArrayList<>();

        for (int i = 1; i <= numCall; i++) {
            Call call = new Call("BOG1345-" + i);
            calls.add(call);
            dispatcher.dispatchCall(call);
        }

        dispatcher.shutdown();
        while (dispatcher.isAllCallsDone()) ;

        calls.forEach(call -> Assertions.assertNotEquals(Call.Status.PENDING, call.getStatus(), "Call not processed " + call.getId()));
    }

    @Test
    void dispatchCallTestRejected() {
        List<Call> calls = new ArrayList<>();

        for (int i = 1; i <= numCall + numWaiting + 3; i++) {
            Call call = new Call("CAL1345-" + i);
            calls.add(call);
            dispatcher.dispatchCall(call);
        }

        dispatcher.shutdown();
        while (dispatcher.isAllCallsDone()) ;

        Optional optional = calls.stream().filter(call -> call.getStatus().equals(Call.Status.REJECTED_MAX)).findFirst();
        Assertions.assertTrue(optional.isPresent(), "There is not a rejected call");
    }
}